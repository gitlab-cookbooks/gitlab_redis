# encoding: UTF-8

default['gitlab_redis']['chef_vault'] = ''
default['gitlab_redis']['chef_vault_item'] = ''
default['gitlab_redis']['cache_master_name'] = 'gitlab_redis_cache'
default['gitlab_redis']['cache_sentinels'] = {}
