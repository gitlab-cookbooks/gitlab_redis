# encoding: UTF-8

# Cookbook:: gitlab_redis
# Recipe:: default
#
# Copyright:: 2017, GitLab B.V., MIT.
# rubocop:disable BlockLength

execute "reconfigure-gitlab" do
  command "gitlab-ctl reconfigure"
  action :nothing
end
