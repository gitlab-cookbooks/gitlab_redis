# encoding: UTF-8

# Cookbook:: gitlab_redis
# Recipe:: client-cache
#
# Copyright:: 2017, GitLab B.V., MIT.
# rubocop:disable BlockLength

include_recipe 'chef-vault'
redis_secrets = chef_vault_item(node['gitlab_redis']['chef_vault'], node['gitlab_redis']['chef_vault_item'])

include_recipe 'gitlab_redis::default'

template '/etc/gitlab/redis/client-cache.yml' do
  source 'redis-client.erb'
  owner 'root'
  group 'root'
  mode '0600'
  variables redis: {
    'master_name' => node['gitlab_redis']['cache_master_name'],
    'password' => redis_secrets['redis_password'],
    'sentinels' => node['gitlab_redis']['cache_sentinels']
  }
  notifies :run, 'execute[reconfigure-gitlab]', :delayed
end
