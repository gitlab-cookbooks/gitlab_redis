# encoding: UTF-8

name             'gitlab_users'
maintainer       'GitLab'
maintainer_email 'ops-contact@gitlab.com'
license          'MIT'
description      'Redis management for GitLab.com'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.0.1'
chef_version     '>= 12.1' if respond_to?(:chef_version)
issues_url       'https://gitlab.com/gitlab-cookbooks/gitlab_redis/issues'
source_url       'https://gitlab.com/gitlab-cookbooks/gitlab_redis'

supports 'ubuntu', '= 16.04'
